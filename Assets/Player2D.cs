using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player2D : MonoBehaviour
{
    // Game data
    public GameData gameData;
    // Player data
    public dataDani dataDani;
    // Death at Y > deathInY
    public float deathInY;

    // Start is called before the first frame update
    void Start()
    {
        // Set initials
        this.gameData.overworked = false;
        this.gameData.fallen = false;
        this.gameData.classChange = false;
        this.dataDani.health = 6;
    }

    // Update is called once per frame
    void Update()
    {
        // Death by fall (below Y=-10)
        if (this.transform.position.y < this.deathInY)
        {
            this.dataDani.health = 0;
            this.lowerHealth("fallen");
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "enemy")
        {
            this.dataDani.health--;
            this.lowerHealth("");
        }
        else if (other.transform.tag == "marc")
        {
            dataDani.health--;
            this.lowerHealth("classChange");
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "enemy")
        {
            this.dataDani.health--;
            this.lowerHealth("");
        }
        else if (other.transform.tag == "muertePorTrabajo")
        {
            this.dataDani.health = 0;
            this.lowerHealth("overworked");
        }
    }

    // Checks the current health and sets the type of death if ded
    void lowerHealth(string deathType)
    {
        if (this.dataDani.health <= 0)
        {
            switch (deathType)
            {
                case "fallen":
                    this.gameData.fallen = true;
                    break;
                case "overworked":
                    this.gameData.overworked = true;
                    break;
                case "classChange":
                    this.gameData.classChange = true;
                    break;
                default:
                    break;
            }
            SceneManager.LoadScene("gameOver");
        }
    }

}

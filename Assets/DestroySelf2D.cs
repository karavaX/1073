using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelf2D : MonoBehaviour
{
    // Check X axis
    public bool useX;
    // Destroy at X coord
    public float destroyAtX;
    // check Y axis
    public bool useY;
    // Destroy at Y coord
    public float destroyAtY;
    // Transform component
    protected Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        // Set initial attributes
        this.tf = this.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        // Check X axis no matter the negative
        if (useX && Mathf.Abs(this.tf.position.x) > Mathf.Abs(this.destroyAtX))
        {
            Destroy(this.gameObject);
        }
        // Check Y axis no matter the negative
        if (useY && Mathf.Abs(this.tf.position.y) > Mathf.Abs(this.destroyAtY))
        {
            Destroy(this.gameObject);
        }
    }
}

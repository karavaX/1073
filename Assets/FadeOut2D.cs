using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut2D : MonoBehaviour
{
    protected float time;

    // Start is called before the first frame update
    void Start()
    {
        this.time = 10;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnEnable() {
        StartCoroutine(waitAndDisable());
    }

    IEnumerator waitAndDisable()
    {
        yield return new WaitForSeconds(this.time);
        this.gameObject.SetActive(false);
    }
}

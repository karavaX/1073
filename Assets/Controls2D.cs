using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls2D : MonoBehaviour
{
    public dataDani dataDani;
    protected Animator animator;
    protected Rigidbody2D rigidBody2D;

    // Start is called before the first frame update
    void Start()
    {
        // Set initial attributes
        this.dataDani.jump = true;
        this.dataDani.faceRight = true;
        this.rigidBody2D = this.GetComponent<Rigidbody2D>();
        this.animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // Move right
        if (Input.GetKey(KeyCode.D))
        {
            // Set velocity
            this.rigidBody2D.velocity = new Vector2(this.dataDani.speed, this.rigidBody2D.velocity.y);
            // Check for direction change to flip X sprite
            if (!this.dataDani.faceRight)
            {
                this.GetComponent<SpriteRenderer>().flipX = false;
                this.dataDani.faceRight = true;
            }
        }
        // Move left
        else if (Input.GetKey(KeyCode.A))
        {
            // Set velocity
            this.rigidBody2D.velocity = new Vector2(-this.dataDani.speed, this.rigidBody2D.velocity.y);
            // Check for direction change to flip X sprite
            if (this.dataDani.faceRight)
            {
                this.GetComponent<SpriteRenderer>().flipX = true;
                this.dataDani.faceRight = false;
            }
        }
        // Stop moving
        else
        {
            this.rigidBody2D.velocity = new Vector2(0, this.rigidBody2D.velocity.y);
        }
        // Jump
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W)) && this.dataDani.jump)
        {
            // Add force
            this.rigidBody2D.AddForce(new Vector2(0, this.dataDani.jumpForce), ForceMode2D.Impulse);
            // Set jumpable to false
            this.dataDani.jump = false;
            this.animator.SetBool("jump", true);
        }
        // Set animator velocity
        animator.SetFloat("speed", Mathf.Abs(this.rigidBody2D.velocity.x));
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        // Reset jumpable if it touches the ground
        if (other.transform.tag == "world")
        {
            // Set jumpable to true
            this.dataDani.jump = true;
            animator.SetBool("jump", false);
        }
    }
}

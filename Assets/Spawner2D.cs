using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner2D : MonoBehaviour
{
    // Time between spawns
    public float cooldown;
    // Object to spawn
    public GameObject spawned;
    // Random light constant rotation
    public bool rotation;
    // Game data
    public GameData gameData;
    // Match with game difficulty
    public bool matchDifficulty;

    // Start is called before the first frame update
    void Start()
    {
        // Start spawning
        // StartCoroutine(spawn());
    }

    // Update is called once per frame
    void Update()
    {

    }

    // onEnable is called after being activated/enabled
    void OnEnable()
    {
        // Start spawning
        StartCoroutine(spawn());
    }

    // Spawn coroutine
    IEnumerator spawn()
    {
        while (true)
        {
            // Apply cooldown
            if (this.matchDifficulty)
            {
                yield return new WaitForSeconds(this.cooldown / this.gameData.difficulty);
            }
            else
            {
                yield return new WaitForSeconds(this.cooldown);
            }
            // Create the object
            GameObject sp = Instantiate(this.spawned);
            // Set position to the object based on spawner coords
            float posX = this.GetComponent<Transform>().position.x;
            float posY = this.GetComponent<Transform>().position.y;
            sp.transform.position = new Vector2(posX, posY);
            // If rotation is activated, the small details man
            if (this.rotation)
            {
                float rot = Random.Range(-20, 20);
                sp.GetComponent<Rigidbody2D>().SetRotation(rot);
            }
        }
    }
}

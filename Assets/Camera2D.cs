using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2D : MonoBehaviour
{
    // Lock the camera in the X axis
    public bool lockX;
    // Lock the camera in the Y axis
    public bool lockY;
    // Center the camera to
    public Transform anchor;

    // Start is called before the first frame update
    void Start()
    {
        // none
    }

    // Update is called once per frame
    void Update()
    {
        // Both axis are locked
        if (lockX && lockY)
        {
            this.transform.position = new Vector3(anchor.position.x, anchor.position.y, this.transform.position.z);
        }
        // X axis locked
        else if (lockX)
        {
            this.transform.position = new Vector3(anchor.position.x, this.transform.position.y, this.transform.position.z);
        }
        // Y axis locked
        else if (lockY)
        {
            this.transform.position = new Vector3(this.transform.position.x, anchor.position.y, this.transform.position.z);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTime : MonoBehaviour
{
    // Game data
    public GameData gameData;
    // Delay to add points to score
    public float scoreDelay;
    // How much score to add
    public int scoreIncrease;
    // Delay to increase difficulty
    public float difficultyDelay;
    // How much difficulty to increase
    public float difficultyIncrease;
    protected TMPro.TextMeshProUGUI textMesh;

    // Start is called before the first frame update
    void Start()
    {
        // Set initial attributes
        this.gameData.difficulty = 1;
        this.gameData.score = 0;
        this.textMesh = this.GetComponent<TMPro.TextMeshProUGUI>();
        // Start coroutines
        StartCoroutine(scoreCount());
        StartCoroutine(increaseDifficulty());
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Adds points to the score with delay
    IEnumerator scoreCount()
    {
        while (true)
        {
            // Set delay
            yield return new WaitForSeconds(this.scoreDelay);
            // Add and set score
            this.gameData.score += this.scoreIncrease;
            this.textMesh.text = "SCORE - " + this.gameData.score;
        }

    }

    // Increases the difficulty with delay
    IEnumerator increaseDifficulty()
    {
        while (true)
        {
            // Set delay
            yield return new WaitForSeconds(this.difficultyDelay);
            // Increase difficulty
            this.gameData.difficulty += this.difficultyIncrease;
            print("Difficulty increase > " + this.gameData.difficulty);
        }

    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    // Data dani
    public dataDani dataDani;
    // Array of health bar sprites
    public Sprite[] sprites;
    // Image component
    protected Image img;

    // Start is called before the first frame update
    void Start()
    {
        // Set initial attributes
        this.img = this.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        // Set health bar sprite depending of the health
        switch (this.dataDani.health)
        {
            case 5:
                this.img.sprite = this.sprites[1];
                break;
            case 4:
                this.img.sprite = this.sprites[2];
                break;
            case 3:
                this.img.sprite = this.sprites[3];
                break;
            case 2:
                this.img.sprite = this.sprites[4];
                break;
            case 1:
                this.img.sprite = this.sprites[5];
                break;
        }
    }
}

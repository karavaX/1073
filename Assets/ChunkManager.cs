using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime;

// Minimum 4 chunks requiered
public class ChunkManager : MonoBehaviour
{
    // Element to check position (Player)
    public Transform seek;
    // How many chunks are created
    public int chunks;
    // How many tilemaps the chunk contains
    public int chunkTilemaps;
    // Pointer at the seek's right side (Activate)
    protected int pointerFront;
    // Pointer at the seek's left side (Deactivate)
    protected int pointerBack;
    // Index of the chunks activated in order (Index 0 is always deleted)
    protected List<int> active;

    // Start is called before the first frame update
    void Start()
    {
        // Initialize essential attributes
        this.pointerFront = 30;
        this.pointerBack = 30;
        this.active = new List<int>();
    }

    // Update is called once per frame
    void Update()
    {
        // ACTIVATE CHUNK
        // If the difference between the front pointer and the seeked is 30 or below
        if (this.seek.position.x > this.pointerFront - 30)
        {
            // Get a random number between 1 and chunk size (excludes the main chunk)
            int random = (Random.Range(0, this.chunks) + 1) * 2;
            // Get another random if the current is active
            while (this.transform.GetChild(random).gameObject.activeSelf)
            {
                random = (Random.Range(0, this.chunks) + 1) * 2;
            }
            // Set the position of the chunks at the pointer
            this.transform.GetChild(random).GetComponent<Transform>().position = new Vector3(this.pointerFront, 0, 0);
            this.transform.GetChild(random + 1).GetComponent<Transform>().position = new Vector3(this.pointerFront, 0, 0);
            // Activate the chunks
            for (int i = 0; i < this.chunkTilemaps; i++)
            {
                this.transform.GetChild(random + i).gameObject.SetActive(true);
            }
            // Add the chunk index to the list and move the pointer forward
            this.active.Add(random);
            this.pointerFront += 30;
        }
        // DEACTIVATE CHUNK
        // If the difference between the back pointer and the seeked is 60 or more
        if (this.seek.position.x > this.pointerBack + 60)
        {
            // Deactivate the chunks
            for (int i = 0; i < this.chunkTilemaps; i++)
            {
                this.transform.GetChild(this.active[0] + i).gameObject.SetActive(false);
            }
            // Remove the chunk index from the list and move the pointer forward
            this.active.RemoveAt(0);
            this.pointerBack += 30;
        }
        // DEACTIVATE MAIN (Useless) (Unnecessary) (Waste of code) (EL TOC ME MATA POR DENTRO)
        if (this.seek.position.x > 100)
        {
            this.transform.GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(1).gameObject.SetActive(false);
        }
    }
}

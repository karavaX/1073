using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    // Game data
    public GameData gameData;
    // Game Over Camera
    public Camera GOCamera;
    // Is score text?
    public bool score;
    // Text mesh
    protected TMPro.TextMeshProUGUI textMesh;

    // Start is called before the first frame update
    void Start()
    {
        // Set initial attributes
        this.textMesh = this.GetComponent<TMPro.TextMeshProUGUI>();
        // Score or message
        if (this.score)
        {
            this.textMesh.text = "Final Score - " + this.gameData.score;
            if (this.gameData.overworked)
            {
                this.textMesh.color = new Color(0, 0, 0, 255);
            }
        }
        else
        {
            // Individual death screen by death type
            if (this.gameData.overworked)
            {
                this.textMesh.text = "EL ELOI HA ECLIPSADO TU SOBERBIA";
                this.textMesh.color = new Color(255, 0, 0, 255);
                GOCamera.backgroundColor = new Color(255, 255, 255, 255);
            }
            else if (gameData.fallen)
            {
                this.textMesh.text = "HAS CAIDO ANTE EL TRIBUNAL";
                this.textMesh.color = new Color(150, 150, 0, 255);
            }
            else if (gameData.classChange)
            {
                this.textMesh.text = "AL VELILLAS QUE VAS";
                this.textMesh.color = new Color(0, 0, 150, 255);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}

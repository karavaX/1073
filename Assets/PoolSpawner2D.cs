using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolSpawner2D : MonoBehaviour
{
    // Lock the relative position to
    public Transform anchor;
    // Spawnable area in Y
    public int rangeY;
    // Pool of thesis
    public GameObject pool;
    // Time between spawns
    public float cooldown;
    // Horizontal speed
    public int speed;
    // Game data
    public GameData gameData;
    // Match with game difficulty
    public bool matchDifficulty;
    // Rigid body component
    protected Rigidbody2D rigidBody2D;

    // Start is called before the first frame update
    void Start()
    {
        // Set initial attributes
        this.rigidBody2D = this.GetComponent<Rigidbody2D>();
        // Start coroutine
        StartCoroutine(activateFromPool());
    }

    // Update is called once per frame
    void Update()
    {
        // Set position relative to anchor (+30) and a random Y (Poor efficiency)
        this.GetComponent<Transform>().position = new Vector3(anchor.position.x + 40, Random.Range(-this.rangeY, this.rangeY), 0);
    }

    // Spawn coroutine
    IEnumerator activateFromPool()
    {
        while (true)
        {
            // Apply cooldown
            if (this.matchDifficulty)
            {
                yield return new WaitForSeconds(this.cooldown / this.gameData.difficulty);
            }
            else
            {
                yield return new WaitForSeconds(this.cooldown);
            }
            // Activate the first pool object available
            for (int i = 0; i < this.pool.transform.childCount; i++)
            {
                if (!this.pool.transform.GetChild(i).gameObject.activeSelf)
                {
                    GameObject go = this.pool.transform.GetChild(i).gameObject;
                    // Activate
                    go.SetActive(true);
                    // Set position to object based on spawner coords
                    go.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
                    // Check for difficulty
                    if (this.matchDifficulty)
                    {
                        // Set horizontal speed
                        go.GetComponent<Rigidbody2D>().velocity = new Vector2(this.speed * this.gameData.difficulty, 0);

                    }
                    else
                    {
                        // Set horizontal speed
                        go.GetComponent<Rigidbody2D>().velocity = new Vector2(this.speed, 0);
                    }
                    // An elegant and beautiful break
                    break;
                }
            }
        }
    }
}

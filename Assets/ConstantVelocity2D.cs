using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantVelocity2D : MonoBehaviour
{
    // Constant speed
    public float speed;
    // Match with game difficulty
    public bool matchDifficulty;
    // Game data
    public GameData gameData;
    // Last known difficulty
    protected float lastDiff;
    // Rigid body component
    protected Rigidbody2D rigidBody2D;

    // Start is called before the first frame update
    void Start()
    {
        // Set initial attributes
        this.lastDiff = 0;
        this.rigidBody2D = this.GetComponent<Rigidbody2D>();
        // Apply speed based on difficulty or not
        this.rigidBody2D.velocity = new Vector2(this.speed, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.matchDifficulty)
        {
            // Check for changes in difficulty (EVEEEEEEENTS)
            if (this.lastDiff < this.gameData.difficulty)
            {
                this.rigidBody2D.velocity = new Vector2(this.speed * this.gameData.difficulty, 0);
                this.lastDiff = this.gameData.difficulty;
            }
        }
    }
}

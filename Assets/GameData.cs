using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameData : ScriptableObject
{
    // Score of the game
    public int score;
    // Death by our lord and saviour Mr. White
    public bool overworked;
    // Death by step over
    public bool fallen;
    // Increasing difficulty of the game
    public float difficulty;
    // Death by velillas
    public bool classChange;
}

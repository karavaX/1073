using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class dataDani : ScriptableObject
{
    // Current health
    public int health;
    // Horizontal speed
    public float speed;
    // If jumped
    public bool jump;
    // Jump force
    public float jumpForce;
    // Current looking direction
    public bool faceRight;
}

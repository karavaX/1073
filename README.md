# 1073

Demà son les oposicions y per fi pots agafar el son una mica, però ningu a dit de quina manera

## Risketos base

- [x] **Personatge amb controls de teclat** > El personatge es mou amb WASD
- [x] **Moviments per físiques** > Els moviments s'apliquen al RigidBody2D
- [x] **Implementació de col·lisions** > Personatge, tilemaps y altres incorporen Box Collider
- [x] **Condició de derrota amb GameOver** > Si, el personatje te vida, es pot caure del mapa o altres
- [x] **Instanciació i Destrucció d’objectes per codi** > Els "marcs" i "hectors" funcionen així
- [x] **Control temporal mitjançant corrutines** > Qualsevol Spawner, la score, y altres elements
- [x] **Concepte d’Aleatorietat** > Els chunks a carregar s'agafen aleatoriament
- [x] **Puntuació mostrada per pantalla mitjançant canvas** > Si, tambe el Game Over te canvas
- [x] **Moviment de la càmara, si s’escau.** > Seguiment de càmera

## Risketos addicionals

- [x] **Augment progressiu de la dificultat.** > Si, des del Script de la score
- [x] **Escenes extres** > El GameOver es una escena a part
- [x] **Us avançat de físiques** > Les colisions amb els "marcs" i "hectors", el composite collider i player tunejat
- [x] **Accés a components per codi més enllà del RigidBody i el Transform** > SpriteRenderer, Childs, TexMeshPro...
- [X] **Ús correcte de backgrounds, especialment amb tècniques com Parallax** > No te parallax pero cada chunk te un tilemap de background a -1
- [x] **Ús de Pool** > Les "tesis" i els chunks
- [x] **Tilemaps** > Tot el projecte esta basat en tilemaps
- [x] **Animacions** > El personatge principal te animacions de idle, saltar i caminar (tambe fa flips en X)
- [x] **ScriptableObjects** > Les dades principals es guarden aixi
- [x] **Materials** > Per a que el player no es quedi encastat a la paret
